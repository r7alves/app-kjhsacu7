<?php

namespace App\Services\Product;

use App\Repositories\Contracts\ProductRepositoryInterface;

class ListProductsService
{
    protected $productRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    public function execute()
    {
        return $this->productRepository->getAll();
    }
}
