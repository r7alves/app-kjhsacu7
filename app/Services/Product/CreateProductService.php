<?php

namespace App\Services\Product;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\StockRepositoryInterface;
use App\Repositories\StockRepository;

class CreateProductService
{
    protected $productRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository

    ) {
        $this->productRepository = $productRepository;
    }

    public function execute($data)
    {
        $product = $this->productRepository->save($data);

        return $product;
    }
}
