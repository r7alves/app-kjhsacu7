<?php

namespace App\Services\Stock;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\StockRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class ListStockProductService
{
    protected $stockRepository;

    public function __construct(
        StockRepositoryInterface $stockRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->stockRepository = $stockRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(string $sku)
    {
        try {

            $this->productRepository->getBySku($sku);

            return $this->stockRepository->getAllMovementBySku($sku);
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
