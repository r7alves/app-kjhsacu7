<?php

namespace App\Services\Stock;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\StockRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UpdateStockService
{
    protected $stockRepository;
    protected $productRepository;

    public function __construct(
        StockRepositoryInterface $stockRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->stockRepository = $stockRepository;
        $this->productRepository = $productRepository;
    }

    /**
     *
     * @param string $sku
     * @param array $data
     * @return mixed
     */
    public function execute(string $sku, array $data)
    {
        try {

            $this->productRepository->getBySku($sku);

            $this->stockRepository->checkStock($sku, $data);

            return $this->stockRepository->update($sku, $data);

        } catch(ModelNotFoundException $e) {
            throw $e;
        } catch(Exception $e){
            throw $e;
        }

    }
}
