<?php

namespace App\Services\Stock;

use App\Repositories\Contracts\StockRepositoryInterface;

class ListStockService
{
    protected $stockRepository;

    public function __construct(
        StockRepositoryInterface $stockRepository
    ) {
        $this->stockRepository = $stockRepository;
    }

    public function execute()
    {
        return $this->stockRepository->getAllMovements();
    }
}
