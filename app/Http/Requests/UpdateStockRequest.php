<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|integer',
            'transaction' => 'required|in:out,in'
        ];
    }

    public function messages()
    {
        return [
            'quantity.required' => 'Quantity is required attribute.',
            'quantity.integer' => 'Quantity is an attribute of type integer.',
            'transaction.required' => 'Transaction is required attribute.',
            'transaction.in' => 'Transaction is in or out.',
        ];
    }
}
