<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'required|max:8|unique:products',
            'name' => 'required|max:256',
            'quantity' => 'required|integer|gt:0'
        ];
    }

    public function messages()
    {
        return [
            'sku.required' => 'Sku is required attribute.',
            'sku.max' => 'Sku: Maximum 8 characters.',
            'sku.unique' => 'Sku already exists in the database.',
            'name.required' => 'Name is required attribute.',
            'name.max' => 'Name: Maximum 256 characters.',
            'quantity.required' => 'Quantity is required attribute.',
            'quantity.integer' => 'Quantity is an attribute of type integer.'
        ];
    }
}
