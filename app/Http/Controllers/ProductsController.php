<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Services\Product\CreateProductService;
use App\Services\Product\ListProductsService;
use App\Services\Product\UpdateProductService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{
    /** @var CreateProductService */
    protected $createProductService;

    /** @var ListProductsService */
    protected $listProductsService;

    /** @var UpdateProductService */
    protected $updateProductService;

    public function __construct(
        CreateProductService $createProductService,
        ListProductsService $listProductsService,
        UpdateProductService $updateProductService
    ) {
        $this->createProductService = $createProductService;
        $this->listProductsService = $listProductsService;
        $this->updateProductService = $updateProductService;
    }

    /**
     * @OA\Get(
     *      path="/api/v1/products",
     *      operationId="getAllProducts",
     *      tags={"Products"},
     *      summary="Get list of products",
     *      description="Returns list of products",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      )
     *     )
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {

            return response()->json([
                'data' => $this->listProductsService->execute()
            ]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * @OA\Post(
     *      path="/api/v1/products",
     *      operationId="storeProduct",
     *      tags={"Products"},
     *      summary="Store new product",
     *      description="Returns product data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"sku","name", "quantity"},
     *              @OA\Property(property="sku", type="string", example="001A0000"),
     *              @OA\Property(property="name", type="string", example="Product A"),
     *              @OA\Property(property="quantity", type="integer", example="10"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Product")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Content"
     *      )
     * )
     *
     *
     * @param CreateProductRequest $request
     * @return JsonResponse
     */
    public function store(CreateProductRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'data' => $this->createProductService->execute($request->only(['name', 'sku', 'quantity']))
            ]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/v1/products/{id}",
     *      operationId="updateProduct",
     *      tags={"Products"},
     *      summary="Update existing product",
     *      description="Returns updated product data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Product id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Product B")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Product")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Content"
     *      )
     *
     * )
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateProductRequest $request, int $id): JsonResponse
    {
        try {
            return response()->json([
                'data' => $this->updateProductService->execute($id, $request->only(['name']))
            ]);
        } catch (ModelNotFoundException $m) {
            return response()->json(['message' => $m->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

}
