<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateStockRequest;
use App\Services\Stock\ListStockProductService;
use App\Services\Stock\ListStockService;
use App\Services\Stock\UpdateStockService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


class StocksController extends Controller
{
    /** @var UpdateStockService */
    protected $updateStockService;

    /** @var ListStockProductService */
    protected $listStockProductService;

    /** @var ListStockService */
    protected $listStockService;

    public function __construct(
        UpdateStockService $updateStockService,
        ListStockProductService $listStockProductService,
        ListStockService $listStockService
    ) {
        $this->updateStockService = $updateStockService;
        $this->listStockProductService = $listStockProductService;
        $this->listStockService = $listStockService;
    }

   /**
     * @OA\Get(
     *     path="/api/v1/products/stock-history",
     *     operationId="getAllStockMovements",
     *     tags={"Stock"},
     *     summary="Get all stock transactions",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      )
     *     )
     * @return JsonResponse
     */
    public function stockHistory(): JsonResponse
    {
        try {
            return response()->json([
                $this->listStockService->execute()
            ]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/v1/products/{sku}/stock-history",
     *      operationId="getAllTransactionsBySku",
     *      tags={"Stock"},
     *      summary="Get all transactions by sku",
     *      description="Returns All Transactions By Sku",
     *      @OA\Parameter(
     *          name="sku",
     *          description="Product SKU",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Stock")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      )
     *
     * )
     *
     * @param string $sku
     * @return JsonResponse
     */
    public function productStockTransactions(string $sku): JsonResponse
    {
        try {
            return response()->json([
                $this->listStockProductService->execute($sku),
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/v1/products/{sku}/stock-movement",
     *      operationId="addOrRemoveProductStock",
     *      tags={"Stock"},
     *      summary="Add or remove stock transaction for a product",
     *      description="Returns updated stock data",
     *      @OA\Parameter(
     *          name="sku",
     *          description="Product sku",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"quantity", "transaction"},
     *              @OA\Property(property="quantity", type="integer", example="2"),
     *              @OA\Property(property="transaction", type="string", example="out")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Stock")
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     *
     * )
     *
     * @param string $sku
     * @param UpdateStockRequest $request
     * @return JsonResponse
     */
    public function addOrRemove(string $sku, UpdateStockRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'data' => $this->updateStockService->execute($sku, $request->only(['quantity', 'transaction']))
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
