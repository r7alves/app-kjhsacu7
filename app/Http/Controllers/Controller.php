<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**

 * @OA\Info(
 *   title="app-kJHSacu7 API Documentation",
 *   version="1.0",
 *   @OA\Contact(
 *     email="r19alves@gmail.com",
 *     name="Support Team"
 *   )
 * )
 *
 *
 * @OA\Tag(
 *     name="app-kJHSacu7",
 *     description="API Endpoints of app-kJHSacu7"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
