<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     required={"sku", "name", "quantity"},
 *     title="Product",
 *     description="Product model",
 *     @OA\Xml(
 *         name="Product"
 *     ),
 *     @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *     @OA\Property(property="sku", type="string", readOnly="false", example="00000001"),
 *     @OA\Property(property="name", type="string", readOnly="false", example="Product 1"),
 *     @OA\Property(property="quantity", type="integer", readOnly="false", example="10"),
 *     @OA\Property(property="created_at", type="datetime", readOnly="true", example="2022-03-27T19:08:06.000000Z"),
 *     @OA\Property(property="updated_at", type="datetime", readOnly="true", example="2022-03-27T19:08:06.000000Z"),
 * )
 */
class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sku', 'quantity',
    ];

    /**
     *
     * @return mixed
     */
    public function stock()
    {
        return $this->hasMany(Stock::class, 'sku', 'sku');
    }
}
