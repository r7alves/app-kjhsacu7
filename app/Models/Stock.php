<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     required={"sku", "quantity", "transaction"},
 *     title="Stock",
 *     description="Stock model",
 *     @OA\Xml(
 *         name="Stock"
 *     ),
 *     @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *     @OA\Property(property="sku", type="string", readOnly="false", example="00000001"),
 *     @OA\Property(property="quantity", type="integer", readOnly="false", example="2"),
 *     @OA\Property(property="transaction", type="string", readOnly="false", example="in"),
 *     @OA\Property(property="created_at", type="datetime", readOnly="true", example="2022-03-27T19:08:06.000000Z"),
 *     @OA\Property(property="updated_at", type="datetime", readOnly="true", example="out"),
 * )
 */
class Stock extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku', 'quantity', 'transaction'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'updated_at',
    ];

    /**
     * Get the product that owns the stock.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'sku', 'sku');
    }
}
