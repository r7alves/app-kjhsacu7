<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Stock;
use App\Repositories\Contracts\StockRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StockRepository implements StockRepositoryInterface
{
    public const TRANSACTION_OUT = 'out';
    public const MINIMUM_STOCK_LEVEL = 1;

    /**
     *
     * @return mixed
     */
    public function getAllMovements()
    {
        return Stock::with('product:sku,name')->paginate();
    }

    /**
     *
     * @param string $sku
     * @return Product
     */
    public function getAllMovementBySku(string $sku)
    {
        return Product::where('sku', $sku)->with('stock')->paginate(5);
    }

    /**
     *
     * @param $sku
     * @param array $data
     * @return mixed
     */
    public function update($sku, array $data)
    {
        try {

            $product = Product::where('sku', $sku)->firstOrFail();
            $newQuantityforProduct = $data['transaction'] == self::TRANSACTION_OUT ?
                $product->quantity - $data['quantity'] :
                $product->quantity + $data['quantity'];


            $product->update(['quantity' => $newQuantityforProduct]);

            return $product->stock()->create($data);
        } catch (ModelNotFoundException $e) {

            if ($e instanceof ModelNotFoundException) {
                throw $e;
            }
        }
    }

    /**
     * @param string $sku
     * @param array $data
     * @return mixed
     */
    public function checkStock($sku, array $data)
    {

        $currentStockForSku = Stock::where('sku', $sku)->where('transaction', 'in')->sum('quantity') - Stock::where('sku', $sku)->where('transaction', 'out')->sum('quantity');

        if ($data['transaction'] == self::TRANSACTION_OUT and ($currentStockForSku - $data['quantity']) < self::MINIMUM_STOCK_LEVEL) {
            throw new Exception("Unable to perform the operation, stock is lower than the minimum acceptable");
        }

        return $currentStockForSku;
    }
}
