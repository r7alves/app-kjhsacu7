<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @param
     * @return mixed
     */
    public function getAll()
    {
        return Product::all();
    }

    /**
     * @param $sku
     * @return Product
     */
    public function getBySku(string $sku)
    {
        try {
            return Product::where('sku', $sku)->firstOrFail();
        } catch (ModelNotFoundException $e) {

            if ($e instanceof ModelNotFoundException) {
                throw $e;
            }
        }
    }

    /**
     * @param $product_id
     * @return Product
     */
    public function getById(int $id)
    {
        try {
            return Product::where('id', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {

            if ($e instanceof ModelNotFoundException) {
                throw $e;
            }
        }
    }

    /**
     * @param array $data
     * @return Product
     */
    public function save(array $data)
    {
        return Product::create($data);
    }

    /**
     * @param string $sku
     * @param array $data
     * @return Product
     */
    public function update(int $id, array $data)
    {
        $product = $this->getById($id);

        $product->update($data);

        return $product->refresh();
    }
}
