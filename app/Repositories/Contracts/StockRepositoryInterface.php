<?php

namespace App\Repositories\Contracts;


interface StockRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllMovements();

    /**
     * @param string $sku
     * @return mixed
     */
    public function getAllMovementBySku(string $sku);

    /**
     * @param string $sku
     * @param array $data
     * @return mixed
     */
    public function update(string $sku, array $data);

    /**
     * @param string $sku
     * @param array $data
     * @return mixed
     */
    public function checkStock(string $sku, array $data);

}
