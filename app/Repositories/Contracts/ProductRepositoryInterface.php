<?php

namespace App\Repositories\Contracts;


interface ProductRepositoryInterface
{

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $product_id
     * @return Product
     */
    public function getById(int $id);

    /**
     * @param $sku
     * @return Product
     */
    public function getBySku(string $sku);

    /**
     * @param array $data
     */
    public function save(array $data);

    /**
     * @param int $id
     * @param array $data
     *  @return Product
     */
    public function update(int $id, array $data);


}
