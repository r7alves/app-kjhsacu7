<?php

namespace App\Observers;

use App\Models\Product;
use App\Services\Stock\UpdateStockService;

class ProductObserver
{

    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $data = [
            'sku' => $product->sku,
            'quantity' => $product->quantity,
            'transaction' => 'in'
        ];

        $product->stock()->create($data);

    }
}
