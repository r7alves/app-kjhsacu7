<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_can_be_created(): void
    {
        $product = factory(Product::class)->make(['quantity' => 5]);

        $data = [
            'sku' => $product->sku,
            'name' => $product->name,
            'quantity' => $product->quantity,
        ];

        $response =   $this->json('POST', '/api/v1/products', $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'sku',
                        'quantity',
                        'created_at',
                        'updated_at'

                    ]
                ]
            );

        $this->assertDatabaseHas('products', ['sku' => $product->sku]);
    }

    /** @test */
    public function a_product_cannot_be_created_with_a_quantity_less_than_zero(): void
    {
        $product = factory(Product::class)->make(['quantity' => -1]);

        $data = [
            'sku' => $product->sku,
            'name' => $product->name,
            'quantity' => $product->quantity,
        ];

        $response =   $this->json('POST', '/api/v1/products', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertDatabaseMissing('products', ['sku' => $product->sku]);
    }

    /** @test */
    public function a_product_cannot_be_created_with_an_sku_that_already_exists(): void
    {
        $product = factory(Product::class)->create(['quantity' => 5]);

        $data = [
            'sku' => $product->sku,
            'name' => $product->name,
            'quantity' => $product->quantity,
        ];

        $response =   $this->json('POST', '/api/v1/products', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function a_product_can_be_updated(): void
    {
        $product = factory(Product::class)->create();

        $data = [
            'sku' => $product->sku,
            'name' => $product->name,
        ];

        $response =   $this->json('PUT', "/api/v1/products/$product->id", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'sku',
                        'quantity',
                        'created_at',
                        'updated_at'

                    ]
                ]
            );

            $this->assertDatabaseHas('products', $data);

    }

    /** @test */
    public function can_list_all_products(): void
    {
        factory(Product::class, 5)->create(['quantity' => 5]);

        $response = $this->json('GET', '/api/v1/products');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'id',
                            'name',
                            'sku',
                            'quantity',
                            'created_at',
                            'updated_at'
                        ]
                    ]
                ]
            );
    }
}
