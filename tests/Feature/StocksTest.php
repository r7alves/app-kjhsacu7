<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tests\TestCase;

class StocksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_stock_can_be_changed_for_more(): void
    {
        $product = factory(Product::class)->create(['quantity' => 5]);

        $data = [
            'quantity' => 5,
            'transaction' => 'in'
        ];

        $response = $this->json('POST', "/api/v1/products/$product->sku/stock-movement", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'sku',
                        'transaction',
                        'quantity',
                        'created_at',
                    ]
                ]
            );

        $this->assertDatabaseHas('products', ['sku' => $product->sku, 'quantity' => 10]);
    }

    /** @test */
    public function a_product_stock_can_be_changed_for_less(): void
    {
        $product = factory(Product::class)->create(['quantity' => 10]);

        $data = [
            'quantity' => 5,
            'transaction' => 'out'
        ];

        $response = $this->json('POST', "/api/v1/products/$product->sku/stock-movement", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'sku',
                        'transaction',
                        'quantity',
                        'created_at',
                    ]
                ]
            );

        $this->assertDatabaseHas('products', ['sku' => $product->sku, 'quantity' => 5]);
    }

    /** @test */
    public function a_product_stock_cannot_be_less_than_the_minimum(): void
    {
        $product = factory(Product::class)->create(['quantity' => 5]);

        $data = [
            'quantity' => 5,
            'transaction' => 'out'
        ];

        $response = $this->json('POST', "/api/v1/products/$product->sku/stock-movement", $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function cannot_move_stock_for_product_that_does_not_exist(): void
    {
        $product = factory(Product::class)->make(['quantity' => 5]);

        $data = [
            'quantity' => 5,
            'transaction' => 'out'
        ];

        $response = $this->json('POST', "/api/v1/products/$product->sku/stock-movement", $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_list_all_stock_transactions(): void
    {
        factory(Product::class, 10)->create();

        $response = $this->json('GET', "/api/v1/products/stock-history");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "*" => [
                        'data' => [
                            '*' => [
                                'sku',
                                'quantity',
                                'transaction',
                                'created_at',
                                'product'
                            ]
                        ]
                    ]
                ]
            );
    }

    /** @test */
    public function can_list_all_stock_transactions_by_sku(): void
    {
        $product = factory(Product::class)->create(['quantity' => 1000]);

        factory(Stock::class, 5)->create(['sku' => $product->sku]);

        $response = $this->json('GET', "/api/v1/products/$product->sku/stock-history");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "*" => [
                        'data' => [
                            '*' => [
                                'id',
                                'name',
                                'sku',
                                'quantity',
                                'created_at',
                                'updated_at',
                                'stock'
                            ]
                        ]
                    ]
                ]
            );
    }
}
