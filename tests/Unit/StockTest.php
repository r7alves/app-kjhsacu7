<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Models\Stock;
use App\Repositories\StockRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StockTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_a_stock_transaction_in()
    {
        $product = factory(Product::class)->create([
            'quantity'  => 1
        ]);

        $stock = factory(Stock::class)
            ->create([
                'sku' => $product->sku,
                'quantity' => 1,
                'transaction' => 'in'
            ]);

        (new StockRepository)->update($product->sku, $stock->toArray());

        $this->assertDatabaseHas('stocks', ['sku' => $stock->sku]);
        $this->assertDatabaseHas('products', ['sku' => $stock->sku, 'quantity' => 2]);
    }

    /** @test */
    public function can_create_a_stock_transaction_out()
    {
        $product = factory(Product::class)->create([
            'quantity'  => 1
        ]);

        $stock = factory(Stock::class)
            ->create([
                'sku' => $product->sku,
                'quantity' => 1,
                'transaction' => 'out'
            ]);

        (new StockRepository)->update($product->sku, $stock->toArray());

        $this->assertDatabaseHas('stocks', ['sku' => $stock->sku]);
        $this->assertDatabaseHas('products', ['sku' => $stock->sku, 'quantity' => 0]);
    }

    /** @test */
    public function can_create_a_stock_trought_product_observer()
    {
        $product = factory(Product::class)->create([
            'quantity'  => 10
        ]);

        $this->assertDatabaseHas('products', ['sku' => $product->sku]);
        $this->assertDatabaseHas('stocks', ['sku' => $product->sku]);
    }
}
