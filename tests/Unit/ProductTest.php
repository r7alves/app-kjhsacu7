<?php

namespace Tests\Unit;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_a_product()
    {
        $product = factory(Product::class)->create([
            'quantity'  => 10
        ]);

        $this->assertDatabaseHas('products', ['sku' => $product->sku]);
    }

    /** @test */
    public function can_update_a_product()
    {
        $product = factory(Product::class)->create([
            'quantity'  => 10
        ]);

        $quantity = 11;

        $product->quantity = $quantity;
        $product->save();

        $this->assertDatabaseHas('products', ['sku' => $product->sku, 'quantity' => $quantity]);
    }

}
