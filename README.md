## Desafio Técnico Full-stack / app-kJHSacu7

### O que foi realizado.

Foi criado uma API com os seguintes recursos:
- [x] Cadastro de produto
- [x] Atualização de produto
- [x] Listagem dos produto
- [x] Transação de estoque de um produto
- [x] Listagem das transações de estoque de um produto 
- [x] Listagem das transações do estoque 

### Como foi realizado?

Na construção do projeto, utilizamos algumas boas práticas da orientação a objetos. Utilizamos dos príncipios:
- [x] *Princípio da responsabilidade Única (SRP)*, através da camada de serviços, isolamos cada classe com sua responsabilidade;
- [x] *Princípio aberto/fechado (OCP)*, através da camada de contratos, criamos interfaces, deixando a estratégia de implementação para classes que implementam determinada interface.

Utilizamos 3 camadas para as regras de negócio, sendo o fluxo: 
    -> Controller: recebe a chamada da api, trata se necessário o conteúdo da requisição, e na ação chama a camada de serviço;
    -> Services: isolamos cada recurso(save, update, list) em services específicos;
    -> Repositories: aqui fazemos o acesso e manipulação dos dados.

- As operações relacionadas a produtos são encontradas em:
[ProductRepositoryInterface](app/Repositories/Contracts/ProductRepositoryInterface.php)
[ProductRepository](app/Repositories/ProductRepository.php)


- As operações relacionadas as transações de entrada e saida de produto são encontradas:
[StockRepositoryInterface](app/Repositories/Contracts/StockRepositoryInterface.php)
[StockRepository](app/Repositories/StockRepository.php)


### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- versão php: 7.4
- framework [laravel 7](https://laravel.com/docs/7.x);
- Docker

### Como 'startar' o projeto

##### pre-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:

- Docker >= 20.10.12
- Docker compose >= v2.2.3
- Porta 8000 liberada

Após clonar ou baixar o projeto, entrar na pasta raiz do mesmo e executar os seguintes passos:

1) Copiar o arquivo ```.env.example``` para ```.env```
2) No arquivo ```.env ```, na linha ```DB_PASSWORD=SUA_SENHA```, insira uma senha da sua escolha
3) Executar o comando install.sh para fazer o deploy da aplicação no docker
```bash
./install.sh
```

Segue os comandos contidos em **install.sh**, caso queira executar manualmente.

- Executar o build do projeto
```bash
docker-compose build app
```
- Executar os containers em segundo plano:
```bash
docker-compose up -d
```
- Instalar as dependências do aplicativo
```bash
docker-compose exec app composer install
```
- Aguardar container do MySQL iniciar
```bash
sleep 60
```
- Gerar chave unica do aplicativo
```bash
docker-compose exec app php artisan key:generate
```
- Executar as migrações 
```bash
docker-compose exec app php artisan migrate:fresh
```

Após os passos acima, três containers são iniciados: **PHP 7.4**, **MySQL 8.0** e **nginx**.

###### o servidor iniciará na porta:8000 - acesse a URL [http://127.0.0.1:8000/api/documentation](http://127.0.0.1:8000/api/documentation) para a documentação da API

#### 🎲 Rodando os testes

Para rodar os testes unitários e de feature, execute o comando abaixo:

```bash
docker-compose exec app vendor/bin/phpunit
```


