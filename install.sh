#!/bin/bash

#Executar o build do projeto
sudo docker-compose build app

#Iniciar o ambiente descrito no docker-compose.yaml:
docker-compose up -d

#Instalar as dependências do aplicatio
docker-compose exec app composer install

#Aguardar container do MySQL iniciar
sleep 60

#Gerar chave unica do aplicativo
docker-compose exec app php artisan key:generate

#Executar as migrations necessárias
docker-compose exec app php artisan migrate:fresh

#Executando os testes unitários e de feature
docker-compose exec app vendor/bin/phpunit
