<?php

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\StocksController;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {

    Route::post('/products', [ProductsController::class, 'store']);
    Route::put('/products/{id}', [ProductsController::class, 'update']);
    Route::get('/products', [ProductsController::class, 'index']);

    Route::post('/products/{sku}/stock-movement', [StocksController::class, 'addOrRemove']);
    Route::get('/products/{sku}/stock-history', [StocksController::class, 'productStockTransactions']);
    Route::get('/products/stock-history', [StocksController::class, 'stockHistory']);

});
